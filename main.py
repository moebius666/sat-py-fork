# main.py - Main script for solving 3SAT problems
#
# Artificial Intelligence and Decision Systems
# Winter Semester - 2013/2014
# Prof. Rodrigo Ventura
# Prof. Luis Manuel Marques Custodio
#
# Afonso Teodoro 67529 ; MEEC
# Sebastiao Miranda 67713 ; MEEC

import dimacs  # dimacs read/write utility functions
import SAT     # walkSAT, GSAT and DPLL 
import sys     # Lib to read command line arguments
import time    # Lib to profile time

# Verbose Flag:

verbose = -1 # Set a value from -1(silent) to 3(very verbose) 

# Documentation
#
# 1) Example calls:
#
    # Example call for walkSAT
    # main.py ./uf50-218/uf50-01.cnf ./uf50-218/uf50-01_out_walkSAT_p50_mf5000.cnf walkSAT 0.5 1000

    # Example call for GSAT
    # main.py ./uf50-218/uf50-01.cnf ./uf50-218/uf50-01_out_walkSAT_mr10_mc500.cnf GSAT 10 100

    # Example call for DPLL
    # main.py ./uf50-218/uf50-01.cnf ./uf50-218/uf50-01_out_DPLL.cnf DPLL

    # Example call for test batch of 'uf50-0X' files
    # main.py --run-tests 20 25

#
# 2) To Run without command line arguments, uncomment one of the following lines:
#
    # sys.argv = ['main.py','./uf50-218/uf50-01.cnf','./uf50-218/uf50-01_out_walkSAT_p50_mf5000.cnf','walkSAT',0.5,1000]
    # sys.argv = ['main.py','./uf50-218/uf50-01.cnf','./uf50-218/uf50-01_out_walkSAT_mr10_mc500.cnf','GSAT',10,100]
    # sys.argv = ['main.py','./uf50-218/uf50-01.cnf','./uf50-218/uf50-01_out_DPLL','DPLL']
    # sys.argv = ['main.py', '--run-tests', '25', '26']

#
# 3) The main function of this script is 'main()'.
#


##### printUsage #####
# [purpose]: Function to print usage instructions to terminal
#
# [input]: None
#
# [output]: Prints Usage to terminal
#
def printUsage():
    print('Usage 0: main.py INPUT_FILE OUTPUT_FILE ALGORITHM [OPTION1] [OPTION2]')
    print('Algorithms: DPLL, GSAT, walkSAT')
    print('Options(DPLL): None')
    print('Options(GSAT): OPTION1 = max_restarts, OPTION2 = max_climbs')
    print('Options(walkSAT): OPTION1 = p (0<=p<=1) , OPTION2 = max_flips')
    print('Usage 1: main.py --run-tests <first_file_index> <last_file_index>')

##### main #####
# [purpose]: Main function of the program. Calls one of the three algorithms
#   walkSAT, GSAT or DPLL according to command line arguments (sys.argv). This
#   function calls dimacs.readDimacs to read input 3-SAT sentence and
#   calls dimacs.writeDimacs to write the results.
#
def main():
    
    if(len(sys.argv)<4):
        printUsage()
        return   

    # Read input file
    problem = dimacs.readDimacs(sys.argv[1])

    if problem is not None:
        sentence = (problem[2], int(problem[0]), int(problem[1]))
        if verbose>=3:
            print('CNF Sentence:')
            print(sentence)
    else:
        print('[Error] Problem reading file. Exiting...')
        return    
        
    outFileName = sys.argv[2]

    ##########
    # GSAT
    ##########
    if(sys.argv[3]=='GSAT'):  

        if(len(sys.argv)<6):
            print('[Error] Missing GSAT options.')
            printUsage()
            return

        # Collect command line arguments
        max_restarts = int(sys.argv[4])
        max_climbs = int(sys.argv[5])

        if verbose>=1:
            print('solver : GSAT ({0},{1})'.format(max_restarts,max_climbs))

        # Execute algorithm and measure execution time    
        start=time.time()
        isSat, sol, seed, restarts, climbs = SAT.GSAT(sentence, max_restarts, max_climbs)
        end=time.time()

        if verbose>=1:
            print('Time   : %fs'%(end-start))
            print('solved : {0}'.format(isSat))
            
        if verbose>=2:
            print("Used seed: {0}".format(seed))
        
        if verbose>=2 and sol is not None:
            SAT.printSolution(sentence,sol)

        # Pack results and write output file   
        results = ('GSAT', sentence, isSat, sol, end-start, [seed, restarts, climbs])
        dimacs.writeDimacs(outFileName, results)

    ##########
    # walkSAT
    ##########
    elif(sys.argv[3]=='walkSAT'):  

        if(len(sys.argv)<6):
            print('[Error] Missing walkSAT options.')
            printUsage()
            return    

        # Collect command line arguments
        p = float(sys.argv[4])
        max_flips = int(sys.argv[5])

        if verbose>=1:
            print('solver : walkSAT ({0},{1})'.format(p,max_flips))

        # Execute algorithm and measure execution time        
        start=time.time()
        isSat, sol, seed, flips = SAT.walkSAT(sentence, p, max_flips)
        end=time.time()

        if verbose>=1:
            print('Time   : %fs'%(end-start))
            print('flips  : {0}'.format(flips))
            print('solved : {0}'.format(isSat))
        if verbose>=2:
            print("Used seed: {0}".format(seed))

        if verbose>=2 and sol is not None:
            SAT.printSolution(sentence,sol)

        # Pack results and write output file   
        results = ('walkSAT', sentence, isSat, sol, end-start, [seed, flips])
        dimacs.writeDimacs(outFileName, results)

    ##########
    # DPLL
    ##########   
    elif(sys.argv[3]=='DPLL'):  

        if verbose>=1:
            print('solver : DPLL')

        # Execute algorithm and measure execution time        
        start=time.time()
        sol = SAT.DPLL_SAT(sentence)
        end=time.time()

        if verbose>=1:
            print('Time   : %fs'%(end-start))
            print('solved : {0}'.format(bool(sol)))
        if verbose>=2 and sol != False:
            SAT.printSolution(sentence,sol)        

        # Pack results and write output file   
        results = ('DPLL', sentence, bool(sol), sol, end-start, [])
        dimacs.writeDimacs(outFileName, results)

    else:
        print('[Error] Unrecognized Algorithm.')
        printUsage()
        return

##### runTests #####
# [purpose]: Function that runs some pre-made tests.
#   To use this function, call program with
#   '--run-test <clause_range_inf> <clause_range_sup>'.
#
#   For example, with clause_range_inf = 20 and clause_range_sup = 300
#   program will run from file uf50-020.cnf to uf50-0300.cnf
#
def runTests(clause_range_inf,clause_range_sup):

    if len(sys.argv) < 2:
        printUsage()
        return

    nvar = 50

    # DPLL
    for k in range(clause_range_inf,clause_range_sup+1):

        # Build file name
        fname = 'uf50-0' + str(k) 
        ext = '.cnf'

        # Call DPLL
        if verbose>=0:
            print('*** DPLL( '+fname+' ) ***')
        sys.argv = ['main.py','./uf50-218/'+fname+ext,'./uf50-218/'+fname+'_out_DPLL'+ext,'DPLL']
        main()
        
    # WalkSAT
    for k in range(clause_range_inf,clause_range_sup+1):
        
        # Build file name
        fname = 'uf50-0' + str(k) 
        ext = '.cnf'

        # Call walkSAT
        p = 0.5
        max_flips = 2000
        
        if verbose>=0:
            print('*** walkSAT( '+fname+' ; p=' + str(p) +'; mf=' + str(max_flips) +') ***')
        sys.argv = ['main.py','./uf50-218/'+fname+ext,'./uf50-218/'+fname+'_out_walkSAT'+'_p'+str((int(100*p)))+'_mf'+str(max_flips)+ext,'walkSAT',p,max_flips]
        main()
        
    # GSAT
    for k in range(clause_range_inf,clause_range_sup+1):

        # Build file name
        fname = 'uf50-0' + str(k) 
        ext = '.cnf'

        # Call GSAT
        max_restarts = 15
        max_climbs = 300

        if verbose>=0:
            print('*** GSAT( '+fname+' ; mr=' + str(max_restarts) +'; mc=' + str(max_climbs) +') ***')
        sys.argv = ['main.py','./uf50-218/'+fname+ext,'./uf50-218/'+fname+'_out_GSAT'+'_mr'+str(max_restarts)+'_mc'+str(max_climbs)+ext,'GSAT',max_restarts,max_climbs]
        main()

#
# Set the code for when this module is the main program
#
if __name__ == '__main__':
    
    if len(sys.argv) < 4:
        printUsage()
    else:
        if(sys.argv[1]=='--run-tests'):
            runTests(int(sys.argv[2]), int(sys.argv[3]))
        else:
            main()
